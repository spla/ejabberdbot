import sys
import os
import os.path
import re
from datetime import datetime, timedelta
from mastodon import Mastodon
import psycopg2
import subprocess
import string
import secrets
import shlex
import time
import pdb

def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext

def unescape(s):
    s = s.replace("&apos;", "'")
    return s

def generate_pass():
    
    alphabet = string.ascii_letters + string.digits
    
    while True:
    
        password = ''.join(secrets.choice(alphabet) for i in range(10))
        
        if (any(c.islower() for c in password)
              and any(c.isupper() for c in password)
              and sum(c.isdigit() for c in password) >= 3):
           break
    
    return password

def restart_server():

    ejabberd_restart = subprocess.run([ejabberdctl_path, "restart"])

    return ejabberd_restart.returncode

def register_user(username, passwd):

    ejabberd_new_user = subprocess.run([ejabberdctl_path, "register", username, mastodon_hostname, passwd])

    return ejabberd_new_user.returncode

def show_status():

    day = 0

    hour = 0

    minutes = 0

    seconds = 0

    ejabberd_status = subprocess.run([ejabberdctl_path, "status"])

    ejabberd_uptime = subprocess.run([ejabberdctl_path, "stats", "uptimeseconds"], capture_output=True, text=True)

    uptime = shlex.split(ejabberd_uptime.stdout)

    if type(uptime[0]) == str:

        uptime = int(uptime[0])

        day = uptime // (24 * 3600)

        uptime = uptime % (24 * 3600)

        hour = uptime // 3600

        uptime %= 3600

        minutes = uptime // 60

        uptime %= 60

        seconds = uptime

    return (ejabberd_status.returncode, day, hour, minutes, seconds)

def get_rooms():

    local_room_service = "conference." + mastodon_hostname

    ejabberd_get_rooms = subprocess.run([ejabberdctl_path, "muc_online_rooms", local_room_service], capture_output=True, text=True)

    return (ejabberd_get_rooms, ejabberd_get_rooms.returncode)

def user_info():

    ejabberd_user_sessions_info = subprocess.run([ejabberdctl_path, "user_sessions_info", username, "mastodont.cat"], capture_output=True, text=True)

    return_code = ejabberd_user_sessions_info.returncode

    if ejabberd_user_sessions_info.stdout != '':

        user_sessions_info = shlex.split(ejabberd_user_sessions_info.stdout) 

        user_connection = user_sessions_info[0]

        user_ip = user_sessions_info[1]

        user_port = user_sessions_info[2]

        user_priority = user_sessions_info[3]

        user_node = user_sessions_info[4]

        user_uptime = user_sessions_info[5]

        user_status = user_sessions_info[6]

        user_resources = user_sessions_info[7]

    else:

        user_connection = ''

        user_ip = ''

        user_port = ''

        user_priority = ''

        user_node = ''

        user_uptime = ''

        user_status = ''

        user_resources = ''

    return (return_code, user_connection, user_ip, user_port, user_priority, user_node, user_uptime, user_status, user_resources)

def replying():

    reply = False

    content = cleanhtml(text)
    content = unescape(content)

    try:

        start = content.index("@")
        end = content.index(" ")
        if len(content) > end:

            content = content[0: start:] + content[end +1::]

        neteja = content.count('@')

        i = 0
        while i < neteja :

            start = content.rfind("@")
            end = len(content)
            content = content[0: start:] + content[end +1::]
            i += 1

        question = content.lower()

        query_word = question
        query_word_length = len(query_word)

        if query_word  == 'registre':

            reply = True

        if query_word  == 'estat':

            reply = True

        if query_word == 'sales':

            reply = True

        if query_word == 'info':

            reply = True

        if query_word == 'reinicia':

            reply = True

        return (reply, query_word)

    except ValueError as v_error:

        print(v_error)

def mastodon():

    # Load secrets from secrets file
    secrets_filepath = "secrets/secrets.txt"
    uc_client_id     = get_parameter("uc_client_id",     secrets_filepath)
    uc_client_secret = get_parameter("uc_client_secret", secrets_filepath)
    uc_access_token  = get_parameter("uc_access_token",  secrets_filepath)

    # Load configuration from config file
    config_filepath = "config/config.txt"
    mastodon_hostname = get_parameter("mastodon_hostname", config_filepath)
    bot_username = get_parameter("bot_username", config_filepath)
    ejabberdctl_path = get_parameter("ejabberdctl_path", config_filepath)

    # Initialise Mastodon API
    mastodon = Mastodon(
        client_id = uc_client_id,
        client_secret = uc_client_secret,
        access_token = uc_access_token,
        api_base_url = 'https://' + mastodon_hostname,
    )

    # Initialise access headers
    headers={ 'Authorization': 'Bearer %s'%uc_access_token }

    return (mastodon, mastodon_hostname, bot_username, ejabberdctl_path)

def db_config():

    # Load db configuration from config file
    config_filepath = "config/db_config.txt"
    mastodon_db = get_parameter("mastodon_db", config_filepath)
    mastodon_db_user = get_parameter("mastodon_db_user", config_filepath)
    xmppbot_db = get_parameter("xmppbot_db", config_filepath)
    xmppbot_db_user = get_parameter("xmppbot_db_user", config_filepath)

    return (mastodon_db, mastodon_db_user, xmppbot_db, xmppbot_db_user)

def get_parameter( parameter, file_path ):

    if not os.path.isfile(file_path):
        print("File %s not found, exiting."%file_path)
        sys.exit(0)

    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

def usage():

    print('usage: python ' + sys.argv[0] + ' --play' + ' --en')

###############################################################################
# main

if __name__ == '__main__':

    # usage modes

    if len(sys.argv) == 1:

        usage()

    elif len(sys.argv) >= 2:

        if sys.argv[1] == '--play':

            bot_lang = ''

            if len(sys.argv) == 3:

                if sys.argv[2] == '--ca':

                    bot_lang = 'ca'

                if sys.argv[2] == '--es':

                    bot_lang = 'es'

                if sys.argv[2] == '--fr':

                    bot_lang = 'fr'

                elif sys.argv[2] == '--en':

                    bot_lang = 'en'

            elif len(sys.argv) == 2:

                bot_lang = 'ca'

            if not bot_lang in ['ca', 'es', 'fr', 'en']:

                print("\nOnly 'ca', 'es', 'fr' and 'en' languages are supported.\n")
                sys.exit(0)

            mastodon, mastodon_hostname, bot_username, ejabberdctl_path = mastodon()

            mastodon_db, mastodon_db_user, xmppbot_db, xmppbot_db_user = db_config()

            now = datetime.now()

            notifications = mastodon.notifications()

            if len(notifications) == 0:

                print('No mentions')
                sys.exit(0)

            i = 0

            while i < len(notifications):

                notification_id = notifications[i].id

                if notifications[i].type != 'mention':

                    i += 1

                    print(f'dismissing notification {notification_id}')

                    mastodon.notifications_dismiss(notification_id)

                    continue

                account_id = notifications[i]

                username = notifications[i].account.acct

                status_id = notifications[i].status.id

                text  = notifications[i].status.content

                visibility = notifications[i].status.visibility

                reply, query_word = replying()

                if reply == True:

                    if query_word == 'registre':

                        user_pass = generate_pass()

                        return_code = register_user(username, user_pass)

                        print(return_code)

                        if return_code == 0:

                            toot_text = f'@{username} registrat amb èxit!\n\n'

                            toot_text += f'servidor XMPP: {mastodon_hostname} \n'

                            toot_text += f'usuari: {username}@{mastodon_hostname}\n'

                            toot_text += f'contrasenya: {user_pass}\n\n'

                            toot_text += "nota: aquesta contrasenya s'ha generat aleatòriament però si vols pots canviar-la "

                            toot_text += "desprès d'iniciar sessió des del teu programa o app."

                            mastodon.status_post(toot_text, in_reply_to_id=status_id,visibility='direct')

                            mastodon.notifications_dismiss(notification_id)

                        else:

                            toot_text = f'@{username} error en el registre!'

                            mastodon.status_post(toot_text, in_reply_to_id=status_id,visibility=visibility)

                            mastodon.notifications_dismiss(notification_id)

                    elif query_word == 'estat':

                        return_code, day, hour, minutes, seconds = show_status()

                        if return_code == 0:

                            toot_text = f'@{username}\n\nestat:\n\nel servidor :xmpp: funciona amb normalitat.\n\n'

                            if day == 1:

                                day_str = 'dia'

                            else:

                                day_str = 'dies'

                            toot_text += f'en línia:\n\n{str(day)} {day_str}, {str(hour)} hores, {str(minutes)} minuts i {str(seconds)} segons.'

                            mastodon.status_post(toot_text, in_reply_to_id=status_id,visibility=visibility)

                            mastodon.notifications_dismiss(notification_id)

                        else:

                            toot_text = f'@{username}\n\nel servidor xmpp té algún problema!\n\n'

                            mastodon.status_post(toot_text, in_reply_to_id=status_id,visibility=visibility)

                            mastodon.notifications_dismiss(notification_id)

                    elif query_word == 'sales':

                        ejabberd_get_rooms, return_code = get_rooms()

                        if return_code == 0:

                            rooms_list = shlex.split(ejabberd_get_rooms.stdout)

                            toot_text = f'@{username} sales:\n\n'

                            i = 0

                            while i < len(rooms_list):

                                toot_text += f'xmpp: {str(rooms_list[i])}\n\n'

                                i += 1

                            mastodon.status_post(toot_text, in_reply_to_id=status_id,visibility=visibility)

                            mastodon.notifications_dismiss(notification_id)

                        else:

                            toot_text = f'@{username} cap sala :-(\n\n'

                            mastodon.status_post(toot_text, in_reply_to_id=status_id,visibility=visibility)

                            mastodon.notifications_dismiss(notification_id)

                    elif query_word == 'info':

                        return_code, user_connection, user_ip, user_port, user_priority, user_node, user_uptime, user_status, user_resources = user_info()

                        if user_connection != '':

                            toot_text = f'@{username}\n\ninformació de la teva sessió:\n'

                            toot_text += f'\ntipus de connexió: {str(user_connection)}'

                            toot_text += f'\nIP: {str(user_ip)}'

                            toot_text += f'\nport: {str(user_port)}'

                            toot_text += f'\nprioritat: {str(user_priority)}'

                            toot_text += f'\nnode: {str(user_node)}'

                            toot_text += f'\nen línia: {str(user_uptime)} segons'

                            toot_text += f'\nestat: {str(user_status)}'

                            toot_text += f'\nrecursos: {str(user_resources)}'

                            mastodon.status_post(toot_text, in_reply_to_id=status_id,visibility='direct')

                            mastodon.notifications_dismiss(notification_id)

                        else:

                            toot_text = f'@{username}\nno tens cap client connectat!\n'

                            mastodon.status_post(toot_text, in_reply_to_id=status_id,visibility=visibility)

                            mastodon.notifications_dismiss(notification_id)

                    if query_word == 'reinicia' and username == 'spla':

                        return_code, day, hour, minutes, seconds = show_status()

                        toot_text = f'@{username}' 

                        if return_code == 0:

                            if day == 1:

                                day_str = 'dia'

                            else:

                                day_str = 'dies'

                            toot_text += f'\n\nen línia:\n\n{str(day)} {day_str}, {str(hour)} hores, {str(minutes)} minuts i {str(seconds)} segons.'

                        return_code = restart_server()

                        if return_code == 0:

                            toot_text += f'\n\nreiniciant el servidor...'

                            mastodon.status_post(toot_text, in_reply_to_id=status_id, visibility=visibility)

                            mastodon.notifications_dismiss(notification_id)

                else:

                    mastodon.notifications_dismiss(notification_id)   

                i += 1

        else:

            usage()
