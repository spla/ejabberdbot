import getpass
import os
import sys
import psycopg2
from psycopg2 import sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, asking."%file_path)
        write_parameter( parameter, file_path )
        #sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

def write_parameter( parameter, file_path ):
    if not os.path.exists('config'):
        os.makedirs('config')
    print("Setting up xmppbot parameters...")
    print("\n")
    xmppbot_db = input("xmppbot db name: ")
    xmppbot_db_user = input("xmppbot db user: ")
    mastodon_db = input("Mastodon db name: ")
    mastodon_db_user = input("Mastodon db user: ")


    with open(file_path, "w") as text_file:
        print("xmppbot_db: {}".format(xmppbot_db), file=text_file)
        print("xmppbot_db_user: {}".format(xmppbot_db_user), file=text_file)
        print("mastodon_db: {}".format(mastodon_db), file=text_file)
        print("mastodon_db_user: {}".format(mastodon_db_user), file=text_file)

def create_table(db, db_user, table, sql):

    conn = None

    try:

        conn = psycopg2.connect(database = db, user = db_user, password = "", host = "/var/run/postgresql", port = "5432")
        cur = conn.cursor()

        print("Creating table.. "+table)
        # Create the table in PostgreSQL database
        cur.execute(sql)

        conn.commit()
        print("Table "+table+" created!")
        print("\n")

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

###############################################################################
# main

if __name__ == '__main__':

    # Load configuration from config file
    config_filepath = "config/db_config.txt"
    xmppbot_db = get_parameter("xmppbot_db", config_filepath)
    xmppbot_db_user = get_parameter("xmppbot_db_user", config_filepath)
    mastodon_db = get_parameter("mastodon_db", config_filepath)
    mastodon_db_user = get_parameter("mastodon_db_user", config_filepath)

    ############################################################
    # create database
    ############################################################

    conn = None

    try:

        conn = psycopg2.connect(dbname='postgres',
            user=xmppbot_db_user, host='',
            password='')

        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

        cur = conn.cursor()

        print("Creating database " + xmppbot_db + ". Please wait...")

        cur.execute(sql.SQL("CREATE DATABASE {}").format(
            sql.Identifier(xmppbot_db))
        )
        print("Database " + xmppbot_db + " created!")

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

    ############################################################

    try:

        conn = None

        conn = psycopg2.connect(database = xmppbot_db, user = xmppbot_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

        # Load configuration from config file
        os.remove("config/db_config.txt")

        print("Exiting. Run db-setup again with right parameters")
        sys.exit(0)

    finally:

        if conn is not None:

            conn.close()

            print("\n")
            print("xmppbot parameters saved to db-config.txt!")
            print("\n")

    ############################################################
    # Create needed tables
    ############################################################

    print("Creating table...")

    db = xmppbot_db
    db_user = xmppbot_db_user

    table = "botreplies"
    sql = "create table "+table+" (status_id bigint PRIMARY KEY, query_user varchar(40), status_created_at timestamptz)"
    create_table(db, db_user, table, sql)

    ############################################################

    print("Done!")
    print("Now you can run setup.py!")
    print("\n")
