# ejabberdbot
Python bot that allows in-band registration to ejabberd server from your Mastodon server local users.  

It also post your ejabberd server status and uptime and even its available rooms. 


### Dependencies

-   **Python 3**
-   ejabberd server
-   Postgresql server
-   Mastodon's bot account

### Installation:

**Important**: make sure you grant read only privileges to your Mastodon database's following tables for your ejabberd running server user:  

read only Mastodon database tables: accounts, statuses, mentions.

Within Python Virtual Environment:

1. Run `pip install -r requirements.txt` to install needed Python libraries.

2. Run `python db-setup.py` to setup and create new Postgresql database and needed tables in it.  

3. Run `python setup.py` to get your Mastodon's bot account tokens and to be able to post your server's financial status.

4. Use your favourite scheduling method to set `python xmppbot.py` to run regularly. It will start listening your Mastodon server local users registering requests.  

### Usage:  

- register xmpp account:  

@your_bot_username: register  

- get ejabberd server status:  

@your_bot_username: status  

- get ejabberd server rooms:  

@your_bot_username: rooms

  




